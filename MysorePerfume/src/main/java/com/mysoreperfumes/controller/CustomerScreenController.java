package com.mysoreperfumes.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="screenController")
@ViewScoped
public class CustomerScreenController implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private boolean showMenuButtonPanel;
	private boolean showNewCustomerPanel;
	private boolean showNewPurchasePanel;
	private boolean showExistingCustomerPanel;
	private boolean showReferalCustomerPanel;
	private boolean showAllCustomerPanel;
	private boolean showDailyReportPanel;
	private boolean showEnterCodePanel;
	private boolean isReferenceCodeCorrect;
	private boolean isPurchaseHappens;
	private String dialogTitle;
	
	@PostConstruct
	public void init(){
		this.setShowDailyReportPanel(false);
		this.setShowNewCustomerPanel(false);
		this.setShowAllCustomerPanel(false);
		this.setShowReferalCustomerPanel(false);
		this.setShowExistingCustomerPanel(false);
		this.setShowNewPurchasePanel(false);
		this.setShowEnterCodePanel(false);
		this.setShowMenuButtonPanel(true);
		this.setIsPurchaseHappens(false);
		this.setIsReferenceCodeCorrect(false);
	}
	
	public boolean getShowMenuButtonPanel() {
		return showMenuButtonPanel;
	}
	
	public void setShowMenuButtonPanel(boolean showMenuButtonPanel) {
		this.showMenuButtonPanel = showMenuButtonPanel;
	}
	
	
	public boolean getShowNewPurchasePanel() {
		return showNewPurchasePanel;
	}

	public void setShowNewPurchasePanel(boolean showNewPurchasePanel) {
		this.showNewPurchasePanel = showNewPurchasePanel;
	}

	public boolean getShowNewCustomerPanel() {
		return showNewCustomerPanel;
	}
	
	public void setShowNewCustomerPanel(boolean showNewCustomerPanel) {
		this.showNewCustomerPanel = showNewCustomerPanel;
	}
	
	public boolean getShowExistingCustomerPanel() {
		return showExistingCustomerPanel;
	}
	
	public void setShowExistingCustomerPanel(boolean showExistingCustomerPanel) {
		this.showExistingCustomerPanel = showExistingCustomerPanel;
	}
	
	public boolean getShowReferalCustomerPanel() {
		return showReferalCustomerPanel;
	}
	
	public void setShowReferalCustomerPanel(boolean showReferalCustomerPanel) {
		this.showReferalCustomerPanel = showReferalCustomerPanel;
	}
	
	public boolean getShowAllCustomerPanel() {
		return showAllCustomerPanel;
	}
	
	public void setShowAllCustomerPanel(boolean showAllCustomerPanel) {
		this.showAllCustomerPanel = showAllCustomerPanel;
	}
	
	public boolean getShowDailyReportPanel() {
		return showDailyReportPanel;
	}
	
	public void setShowDailyReportPanel(boolean showDailyReportPanel) {
		this.showDailyReportPanel = showDailyReportPanel;
	}
	
	
	
	public boolean getShowEnterCodePanel() {
		return showEnterCodePanel;
	}

	public void setShowEnterCodePanel(boolean showEnterCodePanel) {
		this.showEnterCodePanel = showEnterCodePanel;
	}

	public String getDialogTitle() {
		return dialogTitle;
	}

	public void setDialogTitle(String dialogTitle) {
		System.out.println("setting dailog title "+dialogTitle);
		this.dialogTitle = dialogTitle;
	}

	
	public void displayReferanceCustomerPanel(){
		this.setDialogTitle("Reference Code");
		this.setShowReferalCustomerPanel(true);
		this.setShowEnterCodePanel(true);
	}
	
	public void displayExistingCustomerPanel(){
		this.setDialogTitle("Self Code");
		this.setShowExistingCustomerPanel(true);
		this.setShowEnterCodePanel(true);
	}

	public boolean getIsReferenceCodeCorrect() {
		return isReferenceCodeCorrect;
	}

	public void setIsReferenceCodeCorrect(boolean isReferenceCodeCorrect) {
		this.isReferenceCodeCorrect = isReferenceCodeCorrect;
	}

	public boolean getIsPurchaseHappens() {
		return isPurchaseHappens;
	}

	public void setIsPurchaseHappens(boolean isPurchaseHappens) {
		this.isPurchaseHappens = isPurchaseHappens;
	}
	
	public void displayDailyReport(){
		this.setShowDailyReportPanel(true);
		this.setShowMenuButtonPanel(false);
	}
	
	public void displayMenuButtonPanel(){
		this.setShowDailyReportPanel(false);
		this.setShowNewCustomerPanel(false);
		this.setShowAllCustomerPanel(false);
		this.setShowReferalCustomerPanel(false);
		this.setShowExistingCustomerPanel(false);
		this.setShowNewPurchasePanel(false);
		this.setShowEnterCodePanel(false);
		this.setShowMenuButtonPanel(true);
		this.setIsPurchaseHappens(false);
		this.setIsReferenceCodeCorrect(false);
	}

}
