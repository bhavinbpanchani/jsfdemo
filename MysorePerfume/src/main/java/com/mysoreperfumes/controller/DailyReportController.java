package com.mysoreperfumes.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.mysoreperfumes.entity.Purchase;
import com.mysoreperfumes.model.PurchaseSummary;
import com.mysoreperfumes.service.PurchaseService;

@ManagedBean(name="reportController")
@ViewScoped
public class DailyReportController {
	
	@ManagedProperty(value="#{purchaseService}")
	PurchaseService purchaseService;
	 
	private Date selectedDate;
	 
	@ManagedProperty(value="#{screenController}")
	private CustomerScreenController screenController;
	 
	private List<PurchaseSummary> purchaseSummaryReport;
	
	@PostConstruct
	public void init(){
		selectedDate = new Date();
	}

	public List<PurchaseSummary> getPurchaseSummaryReport() {
		
		if (purchaseSummaryReport == null){
			List<Purchase> purchaseList = null;
			try {
				purchaseList = purchaseService.getAllPurchase(selectedDate);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			this.purchaseSummaryReport = new ArrayList<PurchaseSummary>();
	
			for (Purchase purchase : purchaseList) {
				PurchaseSummary purchaseSummary = new PurchaseSummary(purchase);
				this.purchaseSummaryReport.add(purchaseSummary);
			}
		}
		return purchaseSummaryReport;
	}

	public void setPurchaseSummaryReport(List<PurchaseSummary> purchaseSummaryReport) {
		this.purchaseSummaryReport = purchaseSummaryReport;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}
	
	public void filterRecords(){
		screenController.setShowDailyReportPanel(true);
		this.setPurchaseSummaryReport(null);
	}

	public CustomerScreenController getScreenController() {
		return screenController;
	}

	public void setScreenController(CustomerScreenController screenController) {
		this.screenController = screenController;
	}
	
	public void savePaid(PurchaseSummary purchaseSummary){
		purchaseSummary.setIsIncentiveReleased(true);
		purchaseService.updatePurchase(purchaseSummary.getPurchase());
	}

}
