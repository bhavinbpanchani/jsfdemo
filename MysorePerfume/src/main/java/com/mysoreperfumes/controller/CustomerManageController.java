package com.mysoreperfumes.controller;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.SelectEvent;

import com.mysoreperfumes.backingbean.AbstractBean;
import com.mysoreperfumes.entity.Customer;
import com.mysoreperfumes.model.CustomerSummary;
import com.mysoreperfumes.model.PurchaseSummary;
import com.mysoreperfumes.service.CustomerService;
import com.mysoreperfumes.service.PurchaseService;

@ManagedBean(name="manageController")
@ViewScoped
public class CustomerManageController extends AbstractBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{customerService}")
    CustomerService customerService;
    
    @ManagedProperty(value="#{purchaseService}")
    PurchaseService purchaseService;
    
    @ManagedProperty(value="#{screenController}")
    private CustomerScreenController screenController;
 
    private List<CustomerSummary> customerSummaryReport;
    private List<CustomerSummary> filteredCustomerSummaryReport;
    private CustomerSummary customerSummary;
    private PurchaseSummary purchaseSummary;
    private CustomerSummary selectedCustomerSummary;
    private CustomerSummary selectedPurchaseSummary;
    private String referredCustomerName;
    private Long referredCustomerPhoneNo;
    private boolean isNewCustomer;
    private boolean isReferalCustomer;
    private boolean isExistingCustomer;
    private String code;
    
    @PostConstruct
    public void init(){
    	this.setIsNewCustomer(false);
    	this.setIsReferalCustomer(false);
    	this.setIsExistingCustomer(false);
    }
    
    public List<CustomerSummary> getCustomerSummaryReport() {
    	
		if (customerSummaryReport == null){
			List<Customer> customerList = null;
			try {
				customerList = customerService.getCustomers();
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.customerSummaryReport = new ArrayList<CustomerSummary>();
	
			for (Customer customer : customerList) {
				CustomerSummary customerSummary = new CustomerSummary(customer);
				this.customerSummaryReport.add(customerSummary);
			}
		}
		
		return customerSummaryReport;
	}

	public void setCustomerSummaryReport(List<CustomerSummary> customerSummaryReport) {
		this.customerSummaryReport = customerSummaryReport;
	}

	public void createNewCustomer(String referenceCode){
    	CustomerSummary customerSummary = customerService.createNewCustomerSummary(referenceCode);
    	PurchaseSummary purchaseSummary = purchaseService.createNewPurchaseSummary(customerSummary);
    	this.setCustomerSummary(customerSummary);
    	this.setPurchaseSummary(purchaseSummary);
    	this.setIsNewCustomer(true);
    	screenController.setShowNewCustomerPanel(true);
    	screenController.setDialogTitle("Create Customer");
    }
    
    public void createNewPurchase(){
    	
    	try{
    		PurchaseSummary purchaseSummary = purchaseService.createNewPurchaseSummary(this.customerSummary);
        	this.setPurchaseSummary(purchaseSummary);
        	screenController.setDialogTitle("Create Purchase");
        	screenController.setShowNewPurchasePanel(true);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
    public void doCancle(){
    	screenController.setShowDailyReportPanel(false);
    	screenController.setShowNewCustomerPanel(false);
    	screenController.setShowAllCustomerPanel(false);
    	screenController.setShowReferalCustomerPanel(false);
    	screenController.setShowExistingCustomerPanel(false);
    	screenController.setShowNewPurchasePanel(false);
    	screenController.setShowEnterCodePanel(false);
    	screenController.setShowMenuButtonPanel(true);
    	screenController.setIsPurchaseHappens(false);
    	screenController.setIsReferenceCodeCorrect(false);
    	this.setCode(null);
    	this.setIsNewCustomer(false);
    	this.setIsExistingCustomer(false);
    	this.setIsReferalCustomer(false);
    }
    
    public void saveCustomerPurchase(){
    	Long phoneNo = this.customerSummary.getPhoneNo();
    	this.setCustomerSummary(customerSummary);
    	String customerName = this.customerSummary.getName();
		String selfCode = this.customerSummary.getSelfCode();
		Double incentiveAmount = purchaseSummary.getIncentiveAmount();
		Integer customerId = this.customerSummary.getCustomerId();
    	
    	if(getIsNewCustomer()){
    		try{
    			customerService.addCustomer(this.customerSummary.getCustomer());
    			getRequestContext().addCallbackParam("isValid", true);
	    	}catch(ConstraintViolationException ex){
	    		if(ex.getLocalizedMessage().contains("phoneno")){
	    			facesComponentError("Phone no is already exist", "phoneno");
	    			getRequestContext().addCallbackParam("isValid", false);
	    			return;
	    		}
	    		if(ex.getLocalizedMessage().contains("selfcode")){
	    			System.out.println("same self code comes generating another one");
	    			String newSelfCode = RandomStringUtils.randomAlphabetic(1).toUpperCase() + RandomStringUtils.randomNumeric(3);
	    			this.customerSummary.setSelfCode(newSelfCode);
	    			customerService.addCustomer(this.customerSummary.getCustomer());
	    			getRequestContext().addCallbackParam("isValid", true);
	    		}
	    		ex.printStackTrace();
	    	}
    	}else{
    		if(customerId > 0){
    			Customer dbCustomer = customerService.getCustomerById(this.customerSummary.getCustomerId());
	    		if(phoneNo != dbCustomer.getPhoneNo()){
	    			System.out.println("phone number is different updating values.");
	        		customerService.updateCustomer(this.customerSummary.getCustomer());
	        		getRequestContext().addCallbackParam("isValid", true);
	        		//sendMessageToNewCustomer(phoneNo, customerName, selfCode);
	        	}
    		}
    	}
    	
    	try{
        	if(purchaseSummary.getPurchaseAmount() != null){
	        	
	    		String incentiveAmountStr = "";
	    		if(incentiveAmount != null){
	    			incentiveAmountStr = incentiveAmount.toString();
	    		}
	    		
	        	if(getIsNewCustomer() && !getIsExistingCustomer() && !getIsReferalCustomer()){
	        		//sendMessageToNewCustomer(phoneNo, customerName, selfCode);
	        	}else if (!getIsNewCustomer() && getIsExistingCustomer() && !getIsReferalCustomer()){
	        		//sendMessageToExistingCustomer(phoneNo, customerName, incentiveAmountStr);
	        	}else if(getIsNewCustomer() && !getIsExistingCustomer() && getIsReferalCustomer()){
	        		//sendMessageToReferalCustomer(phoneNo, customerName, selfCode, incentiveAmountStr);
	        		submitIncentive();
	        	}
	        	purchaseService.addPurchase(this.purchaseSummary.getPurchase());
	        	getRequestContext().addCallbackParam("isValid", true);
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	this.setCustomerSummaryReport(null);
    	doCancle();
    	facesGlobalInfo("Customer has been saved");
    	
    }
    
    public void submitIncentive(){
    	
		String customerName = this.customerSummary.getName();
    		
		String referenceCode = this.getCode();
    	Customer customer = customerService.getCustomerBySelfCode(referenceCode);
    	if(customer != null){
    		String referalCustomerName = customer.getName();
    		Double referalIncentiveAmount = this.purchaseSummary.getReferenceIncentiveAmount();
    		Long phoneNo = customer.getPhoneNo();
    		//sendMessageToReferredCustomer(phoneNo, customerName, referalCustomerName, referalIncentiveAmount);
    		this.purchaseSummary.setReferenceCustomerId(customer.getCustomerId());
    		this.purchaseSummary.setReferenceCode(referenceCode);
    	}

    }
    
    
    public void checkReferenceCode(){
    	String referenceCode = this.getCode();
    	Customer customer = customerService.getCustomerBySelfCode(referenceCode);
    	
    	if(customer != null){
    		screenController.setShowEnterCodePanel(false);
    		screenController.setIsReferenceCodeCorrect(true);
    		this.setReferredCustomerName(customer.getName());
    		this.setReferredCustomerPhoneNo(customer.getPhoneNo());
    		this.setIsReferalCustomer(true);
    		this.setIsExistingCustomer(false);
    		createNewCustomer(referenceCode);
    	}else{
    		facesComponentError("Reference code does not exist", "code");
    	}
    }
    
    public void onRowSelect(SelectEvent event){
    	CustomerSummary customerSummary = (CustomerSummary)event.getObject();
    	this.setSelectedCustomerSummary(customerSummary);
    	PurchaseSummary purchaseSummary = purchaseService.createNewPurchaseSummary(customerSummary);
    	this.setPurchaseSummary(purchaseSummary);
    	this.setCustomerSummary(customerSummary);
    	screenController.setShowExistingCustomerPanel(true);
		screenController.setShowEnterCodePanel(false);
		this.setIsExistingCustomer(true);
    	this.setIsReferalCustomer(false);
    	this.setIsNewCustomer(false);
    }
    
    private void sendMessageToNewCustomer(Long phoneNo, String customerName, String selfCode){
    	
    	Properties prop = new Properties();

    	try {
    		
    		String mobileNo = getAppendIndiaCode(phoneNo);

    		// load a properties file
    		prop.load(getClass().getResourceAsStream("/message.properties"));

    		// get the property value and print it out

    		String message = prop.getProperty("new_customer_message");
    		String authKey = prop.getProperty("authkey");
    		message = message.replace("{customer_name}", customerName)
    						 .replace("{self_code}", selfCode);
    		
    		sendMessage(mobileNo, message, authKey);

    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
    
    private void sendMessageToExistingCustomer(Long phoneNo, String customerName, String incentiveAmountStr){
    	
    	Properties prop = new Properties();

    	try {
    		
    		String mobileNo = getAppendIndiaCode(phoneNo);


    		// load a properties file
    		prop.load(getClass().getResourceAsStream("/message.properties"));

    		String message = prop.getProperty("existing_customer_message");
    		String authKey = prop.getProperty("authkey");
    		message = message.replace("{customer_name}", customerName)
    						 .replace("{incentive_amount}", incentiveAmountStr);
    		
    		sendMessage(mobileNo, message, authKey);
    		

    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
    private void sendMessageToReferalCustomer(Long phoneNo, String customerName, String selfCode, String incentiveAmountStr){
    	
    	Properties prop = new Properties();

    	try {
    		
    		String mobileNo = getAppendIndiaCode(phoneNo);

    		// load a properties file
    		prop.load(getClass().getResourceAsStream("/message.properties"));

    		// get the property value and print it out

    		String message = prop.getProperty("referal_customer_message");
    		String authKey = prop.getProperty("authkey");
    		message = message.replace("{customer_name}", customerName)
    						.replace("{self_code}", selfCode)
    						.replace("{incentive_amount}", incentiveAmountStr);
    		
    		sendMessage(mobileNo, message, authKey);

    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
    private void sendMessageToReferredCustomer(Long phoneNo, String customerName, String referalCustomerName, Double referalIncentiveAmount){
    	
    	Properties prop = new Properties();

    	try {
    		
    		String mobileNo = getAppendIndiaCode(phoneNo);

    		// load a properties file
    		prop.load(getClass().getResourceAsStream("/message.properties"));

    		// get the property value and print it out

    		String message = prop.getProperty("refered_customer_message");
    		String authKey = prop.getProperty("authkey");
    		message = message.replace("{customer_name}", customerName)
    						.replace("{refered_customer_name}", referalCustomerName)
    						.replace("{refered_incentive_amount}", referalIncentiveAmount.toString());
    		
    		sendMessage(mobileNo, message, authKey);
    		

    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
  
    public PurchaseSummary getPurchaseSummary() {
		return purchaseSummary;
	}

	public void setPurchaseSummary(PurchaseSummary purchaseSummary) {
		this.purchaseSummary = purchaseSummary;
	}

	public CustomerSummary getCustomerSummary() {
		return customerSummary;
	}

	public void setCustomerSummary(CustomerSummary customerSummary) {
		this.customerSummary = customerSummary;
	}

	public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

	public CustomerScreenController getScreenController() {
		return screenController;
	}

	public void setScreenController(CustomerScreenController screenController) {
		this.screenController = screenController;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean getIsExistingCustomer() {
		return isExistingCustomer;
	}

	public void setIsExistingCustomer(boolean isExistingCustomer) {
		this.isExistingCustomer = isExistingCustomer;
	}

	public boolean getIsNewCustomer() {
		return isNewCustomer;
	}

	public void setIsNewCustomer(boolean isNewCustomer) {
		this.isNewCustomer = isNewCustomer;
	}

	public boolean getIsReferalCustomer() {
		return isReferalCustomer;
	}

	public void setIsReferalCustomer(boolean isReferalCustomer) {
		this.isReferalCustomer = isReferalCustomer;
	}

	public String getReferredCustomerName() {
		return referredCustomerName;
	}

	public void setReferredCustomerName(String referredCustomerName) {
		this.referredCustomerName = referredCustomerName;
	}

	public Long getReferredCustomerPhoneNo() {
		return referredCustomerPhoneNo;
	}

	public void setReferredCustomerPhoneNo(Long referredCustomerPhoneNo) {
		this.referredCustomerPhoneNo = referredCustomerPhoneNo;
	}

	public List<CustomerSummary> getFilteredCustomerSummaryReport() {
		return filteredCustomerSummaryReport;
	}

	public void setFilteredCustomerSummaryReport(
			List<CustomerSummary> filteredCustomerSummaryReport) {
		this.filteredCustomerSummaryReport = filteredCustomerSummaryReport;
	}

	public CustomerSummary getSelectedCustomerSummary() {
		return selectedCustomerSummary;
	}

	public void setSelectedCustomerSummary(CustomerSummary selectedCustomerSummary) {
		this.selectedCustomerSummary = selectedCustomerSummary;
	}
	
}