package com.mysoreperfumes.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysoreperfumes.dao.PurchaseDAO;
import com.mysoreperfumes.entity.Purchase;
import com.mysoreperfumes.model.CustomerSummary;
import com.mysoreperfumes.model.PurchaseSummary;

@Service("purchaseService")
@Transactional(readOnly = true)
public class PurchaseService {
	
	@Autowired
	PurchaseDAO purchaseDAO;

	public PurchaseSummary createNewPurchaseSummary(CustomerSummary customerSummary){
    	
    	PurchaseSummary purchaseSummary = new PurchaseSummary();
    	try{
        	purchaseSummary.getPurchase().setCustomer(customerSummary.getCustomer());
        	purchaseSummary.setPurchaseId(0);
        	purchaseSummary.setIncentiveAmount(null);
        	purchaseSummary.setPurchaseAmount(null);
        	purchaseSummary.setPurchaseDescription(null);
        	Date currentDate = new Date();
        	purchaseSummary.setCreateDate(currentDate);
        	purchaseSummary.setModifiedDate(currentDate);
        	purchaseSummary.setReferenceCustomerId(0);
        	purchaseSummary.setReferenceIncentiveAmount(null);
        	purchaseSummary.setIsIncentiveReleased(false);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return purchaseSummary;
    	
    }
	
	public PurchaseDAO getPurchaseDAO() {
		return purchaseDAO;
	}

	public void setPurchaseDAO(PurchaseDAO purchaseDAO) {
		this.purchaseDAO = purchaseDAO;
	}

	@Transactional(readOnly = false)
    public void addPurchase(Purchase purchase) {
		getPurchaseDAO().addPurchase(purchase);
    }
 
    @Transactional(readOnly = false)
    public void deletePurchase(Purchase purchase) {
    	getPurchaseDAO().deletePurchase(purchase);
    }
 
    @Transactional(readOnly = false)
    public void updatePurchase(Purchase purchase) {
    	getPurchaseDAO().updatePurchase(purchase);
    }

    @Transactional(readOnly = false)
    public List<Purchase> getAllPurchase(Date selectedDate) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(selectedDate);
    	Date startDate = calendar.getTime();
    	calendar.add(Calendar.DATE, 1);
    	Date endDate = calendar.getTime();
    	
    	SimpleDateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy");
    	try {
			startDate = dateFormate.parse(dateFormate.format(startDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return  getPurchaseDAO().getAllPurchase(startDate, endDate);
    }
	
}
