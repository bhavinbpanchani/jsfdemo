package com.mysoreperfumes.service;
 
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysoreperfumes.dao.CustomerDAO;
import com.mysoreperfumes.entity.Customer;
import com.mysoreperfumes.model.CustomerSummary;
import com.mysoreperfumes.model.PurchaseSummary;

@Service("customerService")
@Transactional(readOnly = true)
public class CustomerService {
 
    @Autowired
    CustomerDAO customerDAO;
 
    public CustomerSummary createNewCustomerSummary(){
    	return createCustomerSummary(null);
    }
    
    public CustomerSummary createNewCustomerSummary(String referenceCode){
    	return createCustomerSummary(referenceCode);
    }
    
    private CustomerSummary createCustomerSummary(String referenceCode) {
    	CustomerSummary customerSummary = new CustomerSummary();
    	customerSummary.setCustomerId(0);
    	customerSummary.setName(null); 
    	customerSummary.setAddress(null);
    	customerSummary.setPhoneNo(null);
    	String selfCode = RandomStringUtils.randomAlphabetic(1).toUpperCase() + RandomStringUtils.randomNumeric(3);
    	customerSummary.setSelfCode(selfCode);
    	customerSummary.setReferalCode(referenceCode); 
    	Date currentDate = new Date();
    	customerSummary.setCreateDate(currentDate);
    	customerSummary.setModifiedDate(currentDate);
    	return customerSummary;
	}
	    
    @Transactional(readOnly = false)
    public void addCustomer(Customer customer) {
        getCustomerDAO().addCustomer(customer);
    }
 
    @Transactional(readOnly = false)
    public void deleteCustomer(Customer customer) {
        getCustomerDAO().deleteCustomer(customer);
    }
 
    @Transactional(readOnly = false)
    public void updateCustomer(Customer customer) {
        getCustomerDAO().updateCustomer(customer);
    }
 
    public Customer getCustomerById(int id) {
        return getCustomerDAO().getCustomerById(id);
    }
 
    public Customer getCustomerBySelfCode(String selfCode){
    	return getCustomerDAO().getCustomerBySelfCode(selfCode);
    }

    public List<Customer> getCustomers() {
        return getCustomerDAO().getCustomers();
    }
 
    public CustomerDAO getCustomerDAO() {
        return customerDAO;
    }
 
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
}