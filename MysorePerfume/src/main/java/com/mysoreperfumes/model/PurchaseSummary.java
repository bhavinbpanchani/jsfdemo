package com.mysoreperfumes.model;

import java.util.Date;

import com.mysoreperfumes.entity.Customer;
import com.mysoreperfumes.entity.Purchase;
 
 
public class PurchaseSummary {
 
    private Purchase purchase;
    
    public PurchaseSummary(){
    	if(purchase == null){
    		purchase = new Purchase();
    	}
    }
    
    public PurchaseSummary(Purchase purchase){
    	if(purchase != null){
    		this.purchase = purchase;
    	}
    }
    
    public Integer getPurchaseId() {
		return this.purchase.getPurchaseId();
	}

	public void setPurchaseId(Integer purchaseId) {
		this.purchase.setPurchaseId(purchaseId);
	}

	public Double getPurchaseAmount() {
		return this.purchase.getPurchaseAmount();
	}

	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchase.setPurchaseAmount(purchaseAmount);
	}

	public Double getIncentiveAmount() {
		return this.purchase.getIncentiveAmount();
	}

	public void setIncentiveAmount(Double incentiveAmount) {
		this.purchase.setIncentiveAmount(incentiveAmount);
	}

	public String getPurchaseDescription() {
		return this.purchase.getPurchaseDescription();
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchase.setPurchaseDescription(purchaseDescription);
	}

	public Date getCreateDate() {
		return this.purchase.getCreateDate();
	}

	public void setCreateDate(Date createDate) {
		this.purchase.setCreateDate(createDate);
	}

	public Date getModifiedDate() {
		return this.purchase.getModifiedDate();
	}

	public void setModifiedDate(Date modifiedDate) {
		this.purchase.setModifiedDate(modifiedDate);
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
	
	public Integer getReferenceCustomerId() {
		return this.purchase.getReferenceCustomerId();
	}

	public void setReferenceCustomerId(Integer referenceCustomerId) {
		this.purchase.setReferenceCustomerId(referenceCustomerId);
	}

	public Double getReferenceIncentiveAmount() {
		return this.purchase.getReferenceIncentiveAmount();
	}

	public void setReferenceIncentiveAmount(Double referenceIncentiveAmount) {
		this.purchase.setReferenceIncentiveAmount(referenceIncentiveAmount);
	}
	
	public String getReferenceCode() {
		return this.purchase.getReferenceCode();
	}

	public void setReferenceCode(String referenceCode) {
		this.purchase.setReferenceCode(referenceCode);
	}
	
	public CustomerSummary getCustomerSummary(){
		Customer customer = this.purchase.getCustomer();
		return new CustomerSummary(customer);
	}
	
	public void setCustomerSummary(CustomerSummary customerSummary){
		this.purchase.setCustomer(customerSummary.getCustomer());
	}
	
	public boolean getIsIncentiveReleased() {
		return this.purchase.getIsIncentiveReleased();
	}

	public void setIsIncentiveReleased(boolean isIncentiveReleased) {
		this.purchase.setIsIncentiveReleased(isIncentiveReleased);
	}
	
}