package com.mysoreperfumes.model;

import java.util.Date;

import com.mysoreperfumes.entity.Customer;
 
 
public class CustomerSummary {
 
    private Customer customer;
    
    public CustomerSummary(){
    	if(customer == null){
    		customer = new Customer();
    	}
    }
    
    public CustomerSummary(Customer customer){
    	if(customer != null){
    		this.customer = customer;
    	}
    }
    
    public Integer getCustomerId() {
        return customer.getCustomerId();
    }
 
    public void setCustomerId(Integer customerid) {
        this.customer.setCustomerId(customerid);
    }
 
    public String getName() {
        return this.customer.getName();
    }
 
    public void setName(String name) {
        this.customer.setName(name);
    }
 
    public String getAddress() {
		return this.customer.getAddress();
	}

	public void setAddress(String address) {
		this.customer.setAddress(address);
	}

	public Long getPhoneNo() {
		return this.customer.getPhoneNo();
	}

	public void setPhoneNo(Long phoneNo) {
		this.customer.setPhoneNo(phoneNo);
	}

	public String getSelfCode() {
		return this.customer.getSelfCode();
	}

	public void setSelfCode(String selfCode) {
		this.customer.setSelfCode(selfCode);
	}

	public String getReferalCode() {
		return this.customer.getReferalCode();
	}

	public void setReferalCode(String referalCode) {
		this.customer.setReferalCode(referalCode);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Date getCreateDate() {
		return this.customer.getCreateDate();
	}

	public void setCreateDate(Date createDate) {
		this.customer.setCreateDate(createDate);
	}

	public Date getModifiedDate() {
		return this.customer.getModifiedDate();
	}

	public void setModifiedDate(Date modifiedDate) {
		this.customer.setModifiedDate(modifiedDate);
	}
	
	public String getAccountHoldername() {
		return this.customer.getAccountHoldername();
	}

	public void setAccountHoldername(String accountHoldername) {
		this.customer.setAccountHoldername(accountHoldername);
	}

	public Long getBankAccountNo() {
		return this.customer.getBankAccountNo();
	}

	public void setBankAccountNo(Long bankAccountNo) {
		this.customer.setBankAccountNo(bankAccountNo);
	}

	public String getIfscCode() {
		return this.customer.getIfscCode();
	}

	public void setIfscCode(String ifscCode) {
		this.customer.setIfscCode(ifscCode);
	}

	public String getBranchName() {
		return this.customer.getBranchName();
	}

	public void setBranchName(String branchName) {
		this.customer.setBranchName(branchName);
	}
	
}