package com.mysoreperfumes.entity;
 
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 

@Entity
@Table(name="customer")
public class Customer {
 
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="customerid", unique = true, nullable = false)
    private Integer customerId;
	
	@Column(name="name")
    private String name;
	
	@Column(name="address")
    private String address;
	
	@Column(name="phoneno", unique = true)
    private Long phoneNo;
	
	@Column(name="selfcode", unique = true)
    private String selfCode;
	
	@Column(name="referalcode")
    private String referalCode;
	
	@Column(name="createdate")
    private Date createDate;
	
	@Column(name="modifieddate")
    private Date modifiedDate;
	
	@Column(name="accountholdername")
	private String accountHoldername;
	
	@Column(name="bankaccountno")	
	private Long bankAccountNo;
	
	@Column(name="ifsccode")
	private String ifscCode;
	
	@Column(name="branchname")
	private String branchName;
	
	@OneToMany(mappedBy = "customer")
	private Set<Purchase> purchases;

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getSelfCode() {
		return selfCode;
	}

	public void setSelfCode(String selfCode) {
		this.selfCode = selfCode;
	}

	public String getReferalCode() {
		return referalCode;
	}

	public void setReferalCode(String referalCode) {
		this.referalCode = referalCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Set<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(Set<Purchase> purchases) {
		this.purchases = purchases;
	}

	public String getAccountHoldername() {
		return accountHoldername;
	}

	public void setAccountHoldername(String accountHoldername) {
		this.accountHoldername = accountHoldername;
	}

	public Long getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(Long bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

}