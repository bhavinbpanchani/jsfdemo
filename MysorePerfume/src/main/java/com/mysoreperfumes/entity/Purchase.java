package com.mysoreperfumes.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="purchase")
public class Purchase {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="purchaseid", unique = true, nullable = false)
    private Integer purchaseId;
	
	@Column(name="purchaseamount")
    private Double purchaseAmount;
	
	@Column(name="incentiveamount")
    private Double incentiveAmount;	
	
	@Column(name="purchasedescription")
    private String purchaseDescription;
	
	@Column(name="createdate")
    private Date createDate;
	
	@Column(name="modifieddate")
    private Date modifiedDate;
	
	@Column(name="referencecustomerid")
	private Integer referenceCustomerId;
	
	@Column(name="referenceincentiveamount")
	private Double referenceIncentiveAmount;
	
	@Column(name="referencecode")
	private String referenceCode;
	
	@Column(name="isincentivereleased")
	private boolean isIncentiveReleased;
	
	@ManyToOne
	@JoinColumn(name = "customerid")
	private Customer customer;

	public Integer getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Integer purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public Double getIncentiveAmount() {
		return incentiveAmount;
	}

	public void setIncentiveAmount(Double incentiveAmount) {
		this.incentiveAmount = incentiveAmount;
	}

	public String getPurchaseDescription() {
		return purchaseDescription;
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchaseDescription = purchaseDescription;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getReferenceCustomerId() {
		return referenceCustomerId;
	}

	public void setReferenceCustomerId(Integer referenceCustomerId) {
		this.referenceCustomerId = referenceCustomerId;
	}

	public Double getReferenceIncentiveAmount() {
		return referenceIncentiveAmount;
	}

	public void setReferenceIncentiveAmount(Double referenceIncentiveAmount) {
		this.referenceIncentiveAmount = referenceIncentiveAmount;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public boolean getIsIncentiveReleased() {
		return isIncentiveReleased;
	}

	public void setIsIncentiveReleased(boolean isIncentiveReleased) {
		this.isIncentiveReleased = isIncentiveReleased;
	}
	
}
