package com.mysoreperfumes.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysoreperfumes.entity.Purchase;

@Repository
public class PurchaseDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}	

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	 
	public void addPurchase(Purchase purchase) {
        getSessionFactory().getCurrentSession().save(purchase);
    }
   
    public void deletePurchase(Purchase purchase) {
        getSessionFactory().getCurrentSession().delete(purchase);
    }
 
    public void updatePurchase(Purchase purchase) {
        getSessionFactory().getCurrentSession().update(purchase);
    }

    public List<Purchase> getAllPurchase(Date startDate, Date endDate) {
    	String queryStr = "from Purchase purchase where purchase.createDate >= :startdate and purchase.createDate <= :enddate";
    	Query query =  getSessionFactory().getCurrentSession().createQuery(queryStr);
        query.setDate("startdate", startDate);
        query.setDate("enddate", endDate);
        
        List<Purchase> purchaseList =  query.list();
        
        return purchaseList;
    }
}
